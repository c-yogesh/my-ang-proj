import {Component} from "@angular/core";

@Component({
  selector : 'app-server',
  templateUrl: './server.component.html',
  styles: [`
    .online {
      background: #ccc;
    }
  `]
})

export class ServerComponent{
  serverID = 10;
  serverStatus = 'offline';

  getServerStauts(){
    return this.serverStatus;
  }

  constructor () {
    this.serverStatus = Math.random() > 0.5 ? 'Online' : 'Offline';
  }

  getColor(){
    return this.serverStatus === 'Online' ? 'green' : 'red';
  }
}
