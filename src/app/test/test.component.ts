import { Component } from '@angular/core';

@Component ({
  selector : "app-test",
  templateUrl : './test.component.html',
  styleUrls: ['./test.component.css'],
  styles: [`
    .testbg{
      background: tomato;
        text-transform: capitalize;
        color: #fff;
        margin-bottom: 1px;
        padding: 4px 13px;
    }
  `],

})


export class TestComponent {
  listName = 'first list';
  showList = 'No List';
  lists = [];


  addList(){
    this.lists.push(this.listName);
    this.showList = 'List Added';
  }

  getTextColor(){
   return this.showList === 'List Added' ? 'green' : 'red';
  }

}
